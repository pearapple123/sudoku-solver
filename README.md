# Sudoku-solver

A sudoku solver written in Rust. This project will no longer be regularly maintained as it is merely a learning tool for me and was not designed for production use.

Try out a grid by typing it into `sudoku.txt`, substituting each empty space with a zero, then either run `cargo run`, or run `cargo build` then `./target/debug/sudoku_solver`.

Chances are, for anything but easy grids, the solver will return an error saying the puzzle is not solvable - this is because the solver uses a very simple algorithm to get a solution (if it exists), where it continually scans the grid for possible solutions to each of the empty spaces and automatically fills in spaces where only one number is possible. 

Sadly, this method doesn't seem to work for harder grids where at some point logical deductions no longer fill any more empty spaces, so one must make an educated guess for a space and continue using the first method until a solution is reached, or failing that, backtrack and try another possibility.

I could code this as well, except I can't and I don't really want to.
