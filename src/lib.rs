use std::fs::File;
use std::error::Error;
use std::io::Read;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct SudokuBoard {
    pub board: Vec<u8>,
}

// row and elem are indices so we need to use usize
struct SudokuCell {
    pub row: usize,
    pub elem: usize,
}

impl SudokuCell {
    fn new(vec_index: usize) -> SudokuCell {
        SudokuCell { row: vec_index / 9, elem: vec_index % 9 }
    }

    fn get_index(&self) -> usize {
        9 * self.row + self.elem
    }
}

// produces vector of tuples in a specific order: first tuples with first
// element of v1, then tuples with second element of v1 etc.
// This is important as, for get_box, we want cartesian product tuples
// of the first row in the 3x3 box, then the second and third rows. This is
// because this is the format we are using with the sudoku board - we go
// along a row then go to the next row
fn cartesian(v1: &Vec<usize>, v2: &Vec<usize>) -> Vec<(usize, usize)> {
    let mut v_cart: Vec<(usize, usize)> = Vec::new();
    
    for i in v1 {
        for j in v2 { v_cart.push((*i, *j)); }
    }

    v_cart
}

pub fn read_sudoku_from_file(file: &str) -> Result<Vec<u8>, Box<dyn Error>> {
    let mut sudoku_file = File::open(file)?;

    let mut board_string = String::new();
    let mut board: Vec<u8> = Vec::new();

    sudoku_file.read_to_string(&mut board_string)?;

    for str_num in board_string.trim().split(&[' ', '\n']) {
        let num = str_num.parse::<u8>()?;

        board.push(num);
    }

    Ok(board)
}

// inner vectors describe each row of board
// thus, the index of a number in a vector is its elem number
// and the index of that vector in the super-vector is its
// row number
// TODO: board should throw an error if any columns, rows or boxes contain duplicates
impl SudokuBoard {
    
    pub fn new(input: Vec<u8>) -> SudokuBoard {
        SudokuBoard { board: input }
    }

    pub fn print(&self) {
        let board = &self.board;

        for (i, val) in board.iter().enumerate() {
            if i != 0 && i % 9 == 0 {
                println!();
            }

            print!("{val} ");
        }
    }

    fn get(&self, cell: &SudokuCell) -> Option<u8> {
        let index = cell.get_index();
        
        if index > 80 {
            None
        } else {
            Some(self.board[index])
        }
    }

    fn set(&mut self, cell: &SudokuCell, cell_value: u8) {
        self.board[cell.get_index()] = cell_value;
    }

    fn get_row(&self, row_num: usize) -> Vec<u8> {
        let mut row_vec: Vec<u8> = Vec::new();

        for i in 0..9 {
            row_vec.push(self.get(&SudokuCell { row: row_num, elem: i})
                         .expect("Error while getting row: tried to get value outside of board vector"));
        }

        row_vec
    }

    fn get_col(&self, col_num: usize) -> Vec<u8> {
        if col_num > 8 {
            panic!("Error while getting column: tried to get value outside of board vector");
        }
        
        // shifts the board vector so the column we want is the first column (i.e.
        // makes the element on row 0 of the column the first element of the vector)
        let shifted_board: &[u8] = &self.board[col_num..];
        let shifted_board: Vec<u8> = Vec::from(shifted_board);

        // collect every 9th element in board vector (i.e. element on every row
        // of the column)
        shifted_board
            .into_iter()
            .step_by(9)
            .collect::<Vec<u8>>()
    }

    // gets all values in the cell's 3x3 box
    fn get_box(&self, cell: &SudokuCell) -> Vec<u8> {
        let row = cell.row;
        let col = cell.elem;

        let box_row: Vec<usize> = match row {
            0 | 1 | 2 => vec![0, 1, 2],
            3 | 4 | 5 => vec![3, 4, 5],
            _ => vec![6, 7, 8],
        };

        let box_col: Vec<usize> = match col {
            0 | 1 | 2 => vec![0, 1, 2],
            3 | 4 | 5 => vec![3, 4, 5],
            _ => vec![6, 7, 8],
        };

        // get cartesian prod of box_row and box_col, then reduce to a single vector
        // by getting value at each cell in the 3x3 box
        cartesian(&box_row, &box_col)
            .into_iter()
            .map(|(x, y)| self.get(&SudokuCell { row : x, elem : y } )
                 .expect("Error while getting box: attempted to get a value outside of the board vector") as u8)
            .collect::<Vec<u8>>()
    }

    // TODO: impl Option trait for unsolvable puzzles that cause solution vectors for some cells
    // with duplicate solutions or solutions that are already in the row, column or box so are invalid
    fn get_sols_for_cell(&self, cell: &SudokuCell) -> Vec<u8> {
        // cells without a 0 are numbers already given by that particular sudoku board
        if self.get(cell) != Some(0) {
            return vec![];
        }
        
        let col: Vec<u8> = self.get_col(cell.elem);
        let row: Vec<u8> = self.get_row(cell.row);
        let box_vals: Vec<u8> = self.get_box(cell);
        let all_sols: Vec<u8> = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];

        // collects all elements that aren't in col, row or box_vals
        // equivalent to getting the set diff between all_sols and each of the three other vectors
        // and finding the intersection of the three result vectors
        all_sols
            .into_iter()
            .filter(|n| !col.contains(n) && !row.contains(n) && !box_vals.contains(n))
            .collect::<Vec<u8>>()
    }

    // does one pass over (left to right, top to bottom) of the grid
    // If an empty cell has only one possible value, set that cell equal to that value
    fn solve_pass_over(&self) -> SudokuBoard {
        let mut sudoku_clone = self.clone();
        let board_clone = sudoku_clone.board.clone();
        
        for (i, _) in board_clone.into_iter().enumerate() {
            let current_cell = SudokuCell::new(i);
            let sols_for_current_cell = sudoku_clone.get_sols_for_cell(&current_cell);

            if sols_for_current_cell.len() == 1 {
                sudoku_clone.set(&current_cell, sols_for_current_cell[0]);
            }

        }

        sudoku_clone

    }

    // repeatedly pass over the board
    // as you pass over the board and set more and more values, the possible solutions of other
    // empty cells decrease
    // eventually all empty cells are set to values and you get a solution to your sudoku puzzle
    pub fn solve(&self) -> Option<SudokuBoard> {
        let mut sudoku_solution = self.clone();
        
        while sudoku_solution != sudoku_solution.solve_pass_over() {
            sudoku_solution = sudoku_solution.solve_pass_over();
        }

        if sudoku_solution.board.contains(&0) {
            None
        } else {
            Some(sudoku_solution)
        }

    }

}
