use sudoku_solver::SudokuBoard;
use sudoku_solver::read_sudoku_from_file;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
/*    let my_board = vec![
        5, 3, 0, 0, 7, 0, 0, 0, 0,
        6, 0, 0, 1, 9, 5, 0, 0, 0,
        0, 9, 8, 0, 0, 0, 0, 6, 0,

        8, 0, 0, 0, 6, 0, 0, 0, 3,
        4, 0, 0, 8, 0, 3, 0, 0, 1,
        7, 0, 0, 0, 2, 0, 0, 0, 6,

        0, 6, 0, 0, 0, 0, 2, 8, 0,
        0, 0, 0, 4, 1, 9, 0, 0, 5,
        0, 0, 0, 0, 8, 0, 0, 7, 9,
    ];
     */

    let my_board = read_sudoku_from_file("sudoku.txt")?;
    
    let my_sudoku = SudokuBoard::new(my_board);
    my_sudoku.solve()
        .expect("Error: could not solve puzzle")
        .print();

    Ok(())
}
